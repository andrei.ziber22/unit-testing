// mylib.js
/**
 * Adds 2 numbers together
 * @param {number} a 
 * @param {number} b 
 * @returns number
 */
function add(a, b) {
    return a + b;
  }

  /**
   * subtract b from a
   * @param {number} a 
   * @param {number} b 
   * @returns number
   */
  function subtract(a, b) {
    return a - b;
  }
  
  /**
   * multiply a and b
   * @param {number} a 
   * @param {number} b 
   * @returns number
   */
  function multiply(a, b) {
    return a * b;
  }
  
  /**
   * divide b from a
   * @param {number} a 
   * @param {number} b 
   * @returns number
   */
  function divide(a, b) {
    if (b === 0) {
      throw new Error("ZeroDivision: Division by zero is not allowed.");
    }
    return a / b;
  }
  
  module.exports = {
    add,
    subtract,
    multiply,
    divide,
  };
