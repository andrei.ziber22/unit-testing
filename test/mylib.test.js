// mylib.test.js

const chai = require('chai');
const expect = chai.expect;
const mylib = require('../src/mylib');

describe('mylib', () => {
  before(() => {

  });
  it('should add two numbers', () => {
    expect(mylib.add(5, 3)).to.equal(8);
  });

  it('should subtract two numbers', () => {
    expect(mylib.subtract(5, 3)).to.equal(2);
  });

  it('should multiply two numbers', () => {
    expect(mylib.multiply(5, 3)).to.equal(15);
  });

  it('should divide two numbers', () => {
    expect(mylib.divide(6, 2)).to.equal(3);
  });

  it('should throw an error when dividing by zero', () => {
    expect(() => mylib.divide(5, 0)).to.throw();
  });
});

after(() => {
});